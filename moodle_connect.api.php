<?php

use Drupal\user\Entity\User;

/**
 * @param array $user_data
 * @param User $user
 * @return void
 */
function hook_moodle_connect_user_insert_alter(array &$user_data, User $user) {

}

/**
 * @param array $user_data
 * @param User $user
 * @return void
 */
function hook_moodle_connect_user_update_alter(array &$user_data, User $user) {

}