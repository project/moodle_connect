# Moodle Connect

This module is based on [Drupal Moodle Integration](https://www.drupal.org/project/drupal_moodle_integration), but with
significant improvements to the code base and functionality.


## INTRODUCTION
1. Drupal Moodle Integration modules allow you to integrate with Moodle a and get details based on the user logged in.    
2. Navigate to moodle configuration in configuration section and specify moodle url ('http: //url>'/ http's: //url>) and token.
3. Users can navigate to activities from course-lists.
4. Users can enroll and unenroll from course-lists.
5. Assigned courses has been moved under the Account workflows.
6. Added workflow permissions.
7. Added administrative permissions and extended access-checks to allow user-enrollments to be managed.
8. Reimplemented/abstracted query system as plugins to consolidate code and simplify adding support for new endpoints.
9. Added role-based integration restriction.
10. Added multi-level logging.
11. Added integration toggle.
12. Restricted user insert/update parameters to bare minimum and added alter hooks for implementations specific field handling.
13. Added configurable student-id.

## INSTALLATION & CONFIGURATION
The Drupal configuration requires a Moodle web-service authentication token, so it is recommended that you configure Moodle
first.

### Moodle
You must create a Web Service for Drupal to integrate with.  
1. In you Moodle installation, go to Site Administration > Server > Web Services > Overview (/admin/settings.php?section=webservicesoverview)
and follow the steps provided.  The following bullets are supplemental to the Moodle procedure...
   * You must create a web-services role with capabilities to use REST web-services and manage enrollments.  The easiest 
   approach is to create a role under the Manager archetype and enable rest/web-service related permissions (manage 
   enrollments is implied from the Manager archetype).  MAKE SURE YOUR ROLE CAN ASSIGN THE STUDENT AND AUTHENTICATED USER
   ROLES ('Allow role assignments').  
   * When setting up the External Service, you MUST add functions for each of the endpoints you want to support.  The default
   endpoints:
     * core_course_get_courses
     * core_course_get_courses_by_field
     * core_course_get_contents (activities)
     * core_user_create_users
     * core_user_update_users
     * core_user_get_users (not currently used in default implementation, but available)
     * core_enrol_get_users_courses (user current enrollments)
     * enrol_manual_enrol_users
     * enrol_manual_unenrol_users
   * Any additional services will require plugin implementations.

## EXTENDING
Extending the endpoint plugin implementation is required to support additional services (functions).
1. Create the following class:
/modules/custom/mymodule/src/Plugin/MoodleConnect/Endpoint/CoreCourseGetCategories.php
```php
namespace Drupal\mymodule\Plugin\MoodleConnect\Endpoint;

use Drupal\moodle_connect\Plugin\EndpointPluginBase;

/**
 * Plugin implementation of a moodle_connect endpoint.
 *
 * @Endpoint (
 *   id = "mcep_core_course_get_categories",
 *   label = @Translation("Get Course Categories"),
 *   description = @Translation("Gets available course categories."),
 *   function = "core_course_get_categories",
 *   parameters = {}
 * )
 */
class CoreCourseGetCategories extends EndpointPluginBase {

  // OPTIONAL: Adjust response handling...
  public function response()
  {
    $response = parent::response();
    if (is_array($response) && !empty($response)) {
      // Massage results...
    }
    return $response;
  }
}
```
   * Add parameters (for documentation purposes, not currently validated)
2. Flush Drupal caches

#### Usage
```php
namespace Drupal\mymodule\Services\MyMoodleConnectService;
use Drupal\moodle_connect\MoodleConnectTrait;
class MyMoodleConnectService {
   use MoodleConnectTrait;
   
   public function getCategories() : array
   {
      return $this->defaultRequestHandler('core_course_get_categories');
   }
}
```

### DRUPAL
1. Install and enable the Moodle Connect module.
2. Configure > Web Services > Moodle Settings (/admin/structure/services/moodle/settings)
   * URL, Token, and Student ID are required.  
     TOKEN IS GENERATED IN MOODLE FOR THE SELECTED WEB-SERVICE USER AS PART OF THE WEB_SERVICE SETUP PROCESS.
     Site Administration > Server > Web Services > Manage tokens (/admin/webservice/tokens.php)
