<?php

namespace Drupal\moodle_connect\Controller;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\UrlGenerator;
use Drupal\moodle_connect\MoodleConnectTrait;
use Drupal\moodle_connect\Services\CourseService;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * Defines CourseController class.
 */
class CourseController extends ControllerBase {

  use MoodleConnectTrait;

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Moodle Course Service.
   *
   * @var CourseService
   */
  protected CourseService $courseService;

  /**
   * Construct for Course Controller.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param CourseService $courseService
   *   Moodle course service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CourseService $courseService) {
    $this->entityTypeManager = $entity_type_manager;
    $this->courseService = $courseService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('moodle_connect.course_services'),
    );
  }

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function coursesList(): array
  {

    if (!$this->isEnabled()) {
      return ['#theme' => 'moodle_course_disabled'];
    }

    $user = User::load($this->currentUser()->id());
    $course_list = $this->courseService->getCoursesList();
    $moodle_id = null;
    $enrollments = [];

    if (!empty($user->{'field_moodle_user_id'})) {
      $moodle_id = $user->{'field_moodle_user_id'}->value;
      foreach ($this->courseService->userAssignedCourses() as $assigned_course) {
        $enrollments[] = $assigned_course->id;
      }
    }

    foreach ($course_list as &$course) {
      $course->enrolled = in_array($course->id, $enrollments);
      $course->url = $this->getConfig()->get('url')  . "/course/view.php?id=$course->id";
      $course->content_url = \Drupal::service('url_generator')
        ->generateFromRoute('moodle_connect.course_activities', ['courseid' => $course->id], ['absolute' => TRUE]);
    }

    return [
      '#theme' => 'moodle_course_list',
      '#course_list' => $course_list,
      '#moodle_user_id' => $moodle_id,
      '#attached' => [
        'library' => [
          'moodle_connect/moodle_connect',
        ],
      ],
    ];
  }

  /**
   * Function to enroll course.
   */
  public function userEnrolledCourse(): array
  {
    if (!$this->isEnabled()) {
      return ['#theme' => 'moodle_course_disabled'];
    }

    if (!$user = \Drupal::routeMatch()->getParameter('user')) {
      $user = User::load($this->currentUser()->id());
    }

    $moodle_id = $user->{'field_moodle_user_id'}->value;

    $courses = $this->courseService->userAssignedCourses($user->id(), $moodle_id);

    if ($courses) {
      foreach ($courses as &$course) {
        $course->enrolled = TRUE;
        $course->url = $this->getConfig()->get('url')  . "/course/view.php?id=$course->id";
        $course->content_url = \Drupal::service('url_generator')
          ->generateFromRoute('moodle_connect.course_activities', ['courseid' => $course->id], ['absolute' => TRUE]);
      }
    }

    return [
      '#theme' => 'moodle_course_list',
      '#course_list' => $courses,
      '#moodle_user_id' => $moodle_id,
      '#attached' => [
        'library' => [
          'moodle_connect/moodle_connect',
        ],
      ],
    ];
  }

  /**
   * Function to get list of activities form moodle.
   */
  public function courseGetActivities($courseid): array
  {
    if (!$this->isEnabled()) {
      return ['#theme' => 'moodle_course_disabled'];
    }

    return [
      '#theme' => 'moodle_course_activity',
      '#course_activity' => $this->courseService->getActivities($courseid),
      '#url' => $this->getConfig()->get('url')  . "/course/view.php?id=$courseid",
    ];
  }

  /**
   * Function to Unenrol.
   */
  public function updateEnrollment($action, $userid, $courseid)
  {
    if ($userid != $this->currentUser()->id() && !$this->currentUser()->hasPermission("moodle_connect can {$action}l others")) {
      throw new AccessDeniedException("You are not permitted to perform this action.");
    }

    // Enroll / unenroll
    $this->courseService->updateEnrollment($action, $userid, $courseid);

    // Verify and trigger status message
    $user_enrollments = $this->courseService->userAssignedCourses($userid);
    $course = $this->courseService->getCourse((int) $courseid);
    $this->keyByCourseId($user_enrollments);
    $user_enrollment_ids = array_column($user_enrollments, 'id');
    $status_args = [':course' => $course->fullname];
    if ($action === 'enrol') {
      if (in_array($courseid, $user_enrollment_ids)) {
        $this->messenger()->addStatus(t("You have been enrolled in :course", $status_args), TRUE);
      } else {
        $this->messenger()->addError(t("Failed to enroll you in :course.  Please retry or contact an administrator.", $status_args), TRUE);
      }
    } else {
      if (!in_array($courseid, $user_enrollment_ids)) {
        $this->messenger()->addStatus(t("You have been unenrolled from :course", $status_args), TRUE);
      } else {
        $this->messenger()->addError(t("Failed to unenroll you from :course.  Please retry or contact an administrator.", $status_args), TRUE);
      }
    }

    // Reload page (and display status message triggered above)
    if (\Drupal::request()->isXmlHttpRequest()) { // is ajax
      $response = new AjaxResponse();
      if (!empty($_GET['returnUrl']) && UrlHelper::isValid($_GET['returnUrl'])) {
        $return_url = $_GET['returnUrl'];
      } else {
        $return_url = \Drupal::service('url_generator')
          ->generateFromRoute('moodle_connect.course_list', [], ['absolute' => TRUE]);
      }
      $response->addCommand(new RedirectCommand($return_url));
      return $response;
    }

    return $this->redirect('moodle_connect.course_activities', ['courseid' => $courseid]);
  }

}
