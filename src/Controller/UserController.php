<?php

namespace Drupal\moodle_connect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\moodle_connect\MoodleConnectTrait;
use Drupal\moodle_connect\Services\CourseService;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * Defines CourseController class.
 */
class UserController extends ControllerBase {

  use MoodleConnectTrait;

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entity_type_manager;

  /**
   * The Moodle Course Service.
   *
   * @var CourseService
   */
  protected $course_service;

  protected $user;

  /**
   * Construct for Course Controller.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param CourseService $courseService
   *   Moodle course service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CourseService $courseService) {
    $this->entity_type_manager = $entity_type_manager;
    $this->course_service = $courseService;
    $this->user = \Drupal::routeMatch()->getParameter('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('moodle_connect.course_services'),
    );
  }

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function courses() {

    if (!$this->isEnabled()) {
      return ['#theme' => 'moodle_course_disabled'];
    }

    if (isset($this->user->field_moodle_user_id->value)) {
      $moodle_id = $this->user->field_moodle_user_id->value;
      return [
        '#theme' => 'moodle_course_list',
        '#course_list' => $this->course_service->getCoursesList(),
        '#moodle_user_id' => $moodle_id,
        '#attached' => [
          'library' => [
            'moodle_connect/moodle_connect',
          ],
        ],
      ];
    }
  }

  /**
   * Function to enroll course.
   */
  public function userEnrolledCourse() {

    if (!$this->isEnabled()) {
      return ['#theme' => 'moodle_course_disabled'];
    }

    $user = $this->entity_type_manager->getStorage('user')->load($this->currentUser()->id());
    $moodle_id = $user->{'field_moodle_user_id'}->value;
    return [
      '#theme' => 'moodle_course_list',
      '#course_list' => $this->course_service->userAssignedCourses(),
      '#moodle_user_id' => $moodle_id,
      '#attached' => [
        'library' => [
          'moodle_connect/moodle_connect',
        ],
      ],
    ];
  }

  /**
   * Function to get list of activities form moodle.
   */
  public function courseGetActivities($courseid) {

    if (!$this->isEnabled()) {
      return ['#theme' => 'moodle_course_disabled'];
    }

    return [
      '#theme' => 'moodle_course_activity',
      '#course_activity' => $this->course_service->getActivities($courseid),
    ];
  }

  /**
   * Function to Unenrol.
   */
  public function courseUnenrol($userid, $courseid) {

    if ($this->isEnabled()) {
      if ($userid != $this->currentUser()->id()) {
        throw new AccessDeniedException("You can only unenrol yourself.");
      }
      $this->course_service->courseUnenrol($userid, $courseid);
    }
    
    return $this->redirect('moodle_connect.course_activities', ['courseid' => $courseid]);
  }

}
