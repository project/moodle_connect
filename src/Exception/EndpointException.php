<?php

namespace Drupal\moodle_connect\Exception;

use Drupal\moodle_connect\Plugin\EndpointInterface;
use JetBrains\PhpStorm\Internal\LanguageLevelTypeAware;

class EndpointException extends \RuntimeException
{
  public function __construct(EndpointInterface $plugin, $message = "", $code = 0, \Throwable $previous = null)
  {
    $message = "<pre>" .
      "FUNCTION: " . $plugin->function() .
      "\nPARAMETERS: " . json_encode($plugin->requestParameters()) .
      "\nERROR: $message" .
      "\n\nRAW RESPONSE: " . $plugin->getRawResponse() .
    "</pre>";
    parent::__construct($message, $code, $previous);
  }
}