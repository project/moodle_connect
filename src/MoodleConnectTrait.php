<?php

namespace Drupal\moodle_connect;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\moodle_connect\Plugin\EndpointInterface;
use Drupal\moodle_connect\Plugin\EndpointPluginManager;
use Drupal\user\Entity\User;

trait MoodleConnectTrait
{

  /**
   * @param string $message
   * @param array $context
   * @return void
   */
  protected function debug(string $message, array $context = [])
  {
    Helper::Debug($message, $context);
  }

  /**
   * Common request handler
   *
   * @param string $function
   * @param array $args
   * @param \Drupal\moodle_connect\Plugin\EndpointInterface|null $endpoint
   *
   * @return array
   */
  protected function defaultRequestHandler(string $function, array $args = [], EndpointInterface &$endpoint = NULL): array
  {
    try {
      if ($this->isEnabled()) {
        $endpoint = $this->getEndpointManager()->getEndpointByFunction($function, $args);
        $result = (array) $endpoint->request();
        $this->logActivity('request', compact('function', 'args', 'result'));
        return $result;
      }
    } catch (\Exception $e) {
      $this->handleException($e);
    }
    return [];
  }

  /**
   * @return ImmutableConfig
   */
  public function getConfig() : ImmutableConfig
  {
    return \Drupal::service('config.factory')->get('moodle_connect.settings');
  }

  /**
   * Returns TRUE if the module's required configuration settings have been set.
   *
   * @return bool
   */
  public function isConfigured() : bool
  {
    $required_configs = [
      'url',
      'wstoken',
      'student_role_id',
    ];

    $config = $this->getConfig();

    $is_configured = TRUE;

    $missing = [];
    foreach ($required_configs as $required_config) {
      if (empty($config->get($required_config))) {
        $missing[] = $required_config;
        $is_configured = FALSE;
      }
    }

    if (!$is_configured) {
      \Drupal::logger('moodle_connect')->error("Missing required configuration(s): " . implode(', ', $missing));
      \Drupal::messenger()->addError(t("Module is not properly configured.  Please contact an administrator."));
    }

    return $is_configured;
  }

  public function isEnabled() : bool
  {
    return $this->getConfig()->get('enabled') && $this->isConfigured();
  }

  public function getEndpointManager() : EndpointPluginManager
  {
    return \Drupal::service('moodle_connect.plugin.endpoint_manager');
  }

  /**
   * Reconciles the Moodle user-id from the specified Drupal user-id or current logged-in user.
   *
   * @param int|null $user_id
   * @return int|null
   */
  public function getMoodleUserId(int $user_id = null) : ?int
  {
    $user_id = $user_id ?: \Drupal::currentUser()->id();
    // Make sure we have a full user, not a proxy
    $user = User::load($user_id);
    if (!empty($user->{'field_moodle_user_id'})) {
      return (int) $user->{'field_moodle_user_id'}->value;
    }
    return null;
  }

  /**
   * Logs exception and displays user-friendly non-descript error.
   *
   * @param $exception
   * @return void
   */
  public function handleException($exception)
  {
    Helper::HandleException($exception);
  }

  /**
   * Re-indexes course array by course-id.
   *
   * @param array $courses
   */
  public function keyByCourseId(array &$courses)
  {
    $tmp = [];
    foreach ($courses as $course) {
      $tmp[$course->id] = $course;
    }
    $courses = $tmp;
  }

  /**
   * If logging is enabled, logs activity to watchdog.
   * @param string $activity_type
   * @param array $context
   * @return void
   */
  protected function logActivity(string $activity_type, array $context)
  {
    try {
      if ($this->getConfig()->get('logging_enabled')) {
        $log_level = $this->getConfig()->get('log_level') ?? 1;
        if ($log_level >= Helper::GetActivityTypeLogLevel($activity_type)) {
          $this->maskByKey($context, ['password', 'username']);
          $message = "<pre>Activity: $activity_type" .
            "\n\n" . var_export($context, TRUE) . "</pre>";
          \Drupal::logger('moodle_connect')->info($message, $context);
        }
      }
    } catch (\Exception $e) {
      $this->handleException($e);
    }
  }

  /**
   *
   * @param array $array
   * @param array $keys
   *    Array keys to search for.
   * @param array $replacements
   *    Replacement values, indexed by specified keys.  If no key-replacement is defined, default value ('**** REDACTED ****') is used.
   * @return void
   */
  protected function maskByKey(array &$array, array $keys, array $replacements = [])
  {
    if ($array) {
      foreach ($keys as $search_key) {
        $this->arraySearchReplaceByKey($array, $search_key, $replacements[$search_key] ?? '**** REDACTED ****');
      }
    }
  }

  /**
   * Recursively searches array for a key and replaces the value.
   *
   * @param array $array
   * @param string $search_key
   * @param mixed $replacement_value
   * @return void
   */
  protected function arraySearchReplaceByKey(array &$array, string $search_key, $replacement_value)
  {
    foreach ($array as $key => $value) {
      if ($key === $search_key) {
        $array[$key] = $replacement_value;
        continue;
      }
      if (is_array($value)) {
        $this->arraySearchReplaceByKey($array[$key], $search_key, $replacement_value);
      }
    }
  }


}
