<?php

namespace Drupal\moodle_connect\Services;

use Drupal\moodle_connect\Exception\EndpointException;
use Drupal\moodle_connect\MoodleConnectTrait;

/**
 * Class UserService.
 */
class UserService {

  use MoodleConnectTrait;

  /**
   * Check if Moodle connection is configured.
   */
  public function isConfigured() : bool
  {
    $config = $this->getConfig();
    return $config->get('url') && $config->get('wstoken');
  }

  /**
   * Function to get create users in moodle.
   *
   * @param array $users
   *   An array of users to create to Moodle.
   * @return int
   */
  public function moodleCreateUser(array $users) : ?int
  {
    try {
      $endpoint = null;
      $result = $this->defaultRequestHandler('core_user_create_users', compact('users'), $endpoint);
      $this->logActivity('create_user', compact('users'));
      if (!empty($result[0]->id)) {
        return $result[0]->id;
      } else {
        throw new EndpointException($endpoint, "Failed to create user.");
      }
    } catch (\Exception $e) {
      $this->handleException($e);
    }
    return null;
  }

  /**
   * Function to get delete users in moodle.
   *
   * @param array $userids
   *   An array of users to create to Moodle.
   * @return int
   */
  public function moodleDeleteUser(array $userids)
  {
    try {
      $endpoint = null;
      $result = $this->defaultRequestHandler('core_user_delete_users', compact('userids'), $endpoint);
      $this->logActivity('delete_user', compact('userids'));
      return $result;
    } catch (\Exception $e) {
      $this->handleException($e);
    }
    return null;
  }

  /**
   * Function to update users in moodle.
   *
   * @param array $users
   *   An array of users to update.
   * @param string $activity_type
   * @return array
   */
  public function moodleUpdateUser(array $users, string $activity_type = 'update_user') : array
  {
    $result = $this->defaultRequestHandler('core_user_update_users', compact('users'));
    $this->logActivity($activity_type, compact('users'));
    return $result;
  }

  /**
   * Get users.
   *
   * @param array $additional
   *  Additional filter criteria.
   */
  public function getUsers(array $additional = []): array
  {
    return $this->defaultRequestHandler('core_user_get_users', $additional + [
      ['key' => 'email', 'value' => '%%'],
    ]);
  }

}
