<?php

namespace Drupal\moodle_connect\Services;

use Drupal\moodle_connect\MoodleConnectTrait;

/**
 * Class CourseService.
 */
class CourseService {

  use MoodleConnectTrait;

  /**
   * Returns a single course object.
   *
   * @param int $course_id
   * @return \stdClass|null
   */
  public function getCourse(int $course_id) : ?\stdClass
  {
    $courses = $this->defaultRequestHandler('core_course_get_courses_by_field', [
      'field' => 'id',
      'value' => (int) $course_id
    ]);

    if (!empty($courses['courses'][0])) {
      return $courses['courses'][0];
    }

    return null;
  }

  /**
   * User Courses List.
   */
  public function getCoursesList() : array
  {
    return $this->defaultRequestHandler('core_course_get_courses');
  }

  /**
   * User Assigned Courses.
   */
  public function userAssignedCourses(?int $user_id = null, ?int $moodle_user_id = NULL) : array
  {
    try {

      $moodle_user_id = $moodle_user_id ?: $this->getMoodleUserId($user_id);

      if ($moodle_user_id) {
        return $this->defaultRequestHandler('core_enrol_get_users_courses', [
          'userid' => $moodle_user_id
        ]);
      }
      $this->debug("Invalid user context (not a Moodle user).");
    } catch (\Exception $e) {
      $this->handleException($e);
    }
    return [];
  }

  /**
   * Returns assigned Moodle course-ids.
   *
   * @param int|null $user_id
   * @param int|null $moodle_user_id
   * @return array
   */
  public function userAssignedCourseIds(?int $user_id = null, ?int $moodle_user_id = NULL) : array
  {
    $ids = [];
    if ($assigned_courses = $this->userAssignedCourses($user_id, $moodle_user_id)) {
      $ids = array_column($assigned_courses, 'id');
    }
    return $ids;
  }

  /**
   * Get All list of Activities.
   */
  public function getActivities($course_id) : array
  {
    return $this->defaultRequestHandler('core_course_get_contents', [
      'courseid' => $course_id,
    ]);
  }

  /**
   * Course Unenrol.
   */
  public function updateEnrollment(string $action, int $user_id, int $course_id): array
  {
    try {
      if ($moodle_user_id = $this->getMoodleUserId($user_id)) {

        if (!in_array($action, ['enrol', 'unenrol'])) {
          throw new \InvalidArgumentException("Invalid enrollment action.");
        }

        $result = $this->defaultRequestHandler("enrol_manual_{$action}_users", [
          'enrolments' => [
            [
              'roleid' => $this->getConfig()->get('student_role_id'),
              'userid' => $moodle_user_id,
              'courseid' => $course_id,
            ]
          ]
        ]);

        $this->logActivity('enrol', compact('user_id', 'course_id', 'moodle_user_id'));

        return $result;
      }
      $this->debug("Invalid user context (not a Moodle user).");
    } catch (\Exception $e) {
      $this->handleException($e);
    }
    return [];
  }

}
