<?php

namespace Drupal\moodle_connect\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\moodle_connect\Helper;

/**
 * Configure example settings for this site.
 */
class MoodleSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'moodle_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'moodle_connect.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('moodle_connect.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => $config->get('enabled'),
      '#description' => $this->t('Enable/disable integration'),
    ];

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => t('Moodle Url'),
      '#default_value' => $config->get('url'),
      '#description' => $this->t('Moodle Url'),
    ];

    $form['wstoken'] = [
      '#type' => 'textfield',
      '#title' => t('Moodle Token'),
      '#default_value' => $config->get('wstoken'),
      '#description' => $this->t('Moodle Token'),
    ];

    $form['student_role_id'] = [
      '#type' => 'textfield',
      '#title' => t('Student Role ID'),
      '#default_value' => $config->get('student_role_id'),
      '#description' => $this->t('Moodle student role-id'),
    ];

    $form['logging'] = [
      '#type' => 'details',
      '#title' => t('Logging'),
      '#default_value' => $config->get('enabled'),
      '#description' => $this->t('Enable/disable integration'),
    ];

    $form['logging']['logging_enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Logging Enabled'),
      '#default_value' => $config->get('logging_enabled'),
      '#description' => $this->t('Enable/disable logging'),
    ];

    $form['logging']['log_level'] = [
      '#type' => 'select',
      '#title' => 'Log Level',
      '#options' => Helper::GetLogLevels(),
      '#default_value' => $config->get('log_level'),
      '#description' => $this->t('Log level detail'),
    ];

    $this->addOverridden($form);

    return parent::buildForm($form, $form_state);

  }

  /**
   * Disables the field and adds a tooltip if the field is overridden (e.g. settings.php).
   *
   * @param array &$form
   *
   * @return void
   */
  protected function addOverridden(array &$form)
  {

    $fields = [
      'enabled', 'url', 'wstoken',
      ['logging', 'logging_enabled'],
      ['logging', 'log_level'],
    ];

    foreach ($fields as $field_name) {
      if (is_array($field_name)) {
        $parents = $field_name;
        $field_name = array_pop($parents);
        $element =& NestedArray::getValue($form, $parents);
      } else {
        $element =& $form[$field_name];
      }

      if ($this->isOverridden($field_name)) {
        $element['#disabled'] = TRUE;
        $element['#attributes']['title'] = t('This setting is overridden.');
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable('moodle_connect.settings')
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('url', rtrim($form_state->getValue('url'),'/'))
      ->set('wstoken', $form_state->getValue('wstoken'))
      ->set('student_role_id', (int) $form_state->getValue('student_role_id'))
      ->set('logging_enabled', $form_state->getValue('logging_enabled'))
      ->set('log_level', $form_state->getValue('log_level'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Check if config variable is overridden by the settings.php.
   *
   * @param string $name
   *   SMTP settings key.
   *
   * @return bool
   *   Boolean.
   */
  protected function isOverridden($name) {
    $original = $this->configFactory->getEditable('moodle_connect.settings')->get($name);
    $current = $this->configFactory->get('moodle_connect.settings')->get($name);
    return $original != $current;
  }

}
