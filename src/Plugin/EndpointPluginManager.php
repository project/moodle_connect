<?php

namespace Drupal\moodle_connect\Plugin;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Moodle-connect endpoint plugin manager.
 */
class EndpointPluginManager extends DefaultPluginManager {

  /**
   * Constructs EndpointPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Endpoint',
      $namespaces,
      $module_handler,
      'Drupal\moodle_connect\Plugin\EndpointInterface',
      'Drupal\moodle_connect\Annotation\Endpoint'
    );
    $this->alterInfo('moodle_connect_endpoint_info');
    $this->setCacheBackend($cache_backend, 'moodle_connect_endpoint_plugins');
  }

  /**
   * @throws PluginException
   */
  public function getEndpointById(string $id, array $parameters = []) : EndpointInterface
  {
    return $this->getEndpointBy('id', $id, $parameters);
  }

  /**
   * @throws PluginException
   */
  public function getEndpointByFunction(string $function, array $parameters = [])
  {
    return $this->getEndpointBy('function', $function, $parameters);
  }

  /**
   * Returns endpoint referenced by the specified method-value combination.
   *
   * @param string $method
   *  A method which corresponds to the plugin interface methods (e.g. id or function)
   * @param string $value
   *  The target plugin value returned by the method.
   * @param array $parameters
   * @return EndpointInterface
   * @throws PluginException
   */
  public function getEndpointBy(string $method, string $value, array $parameters = [])
  {

    if (!($plugins = $this->getDefinitions())) {
      throw new PluginException("No endpoint plugins found.");
    }

    if (empty($value)) {
      throw new PluginException("Invalid lookup method value.");
    }

    foreach ($plugins as $plugin_definition) {

      $plugin_instance = $this->createInstance($plugin_definition['id'], compact('parameters'));

      if (!method_exists($plugin_instance, $method)) {
        throw new PluginException("Invalid lookup method: {$method}");
      }

      /** @var EndpointInterface $instance */
      if ($plugin_instance->$method() === $value) {
        return $plugin_instance;
      }
    }

    throw new PluginException("Endpoint plugin $method:$value not found.");

  }

}
