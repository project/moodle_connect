<?php

namespace Drupal\moodle_connect\Plugin\Endpoint;

use Drupal\moodle_connect\Plugin\EndpointPluginBase;

/**
 * Plugin implementation of a moodle_connect endpoint.
 *
 * @Endpoint (
 *   id = "mcep_core_enrol_get_users_courses",
 *   label = @Translation("Get User Enrollments"),
 *   description = @Translation("Gets courses the specified user is enrolled in."),
 *   function = "core_enrol_get_users_courses",
 *   parameters = {
 *      "userid" = "User ID"
 *   }
 * )
 */
class CoreEnrolGetUsersCourses extends EndpointPluginBase {

}
