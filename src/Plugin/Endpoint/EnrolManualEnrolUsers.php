<?php

namespace Drupal\moodle_connect\Plugin\Endpoint;

use Drupal\moodle_connect\Plugin\EndpointPluginBase;

/**
 * Plugin implementation of a moodle_connect endpoint.
 *
 * @Endpoint (
 *   id = "mcep_enrol_manual_enrol_users",
 *   label = @Translation("Erol User"),
 *   description = @Translation("Enrols specified user into the specified course."),
 *   function = "enrol_manual_enrol_users",
 *   parameters = {
 *      "userid" = "User ID",
 *      "courseid" = "Course ID"
 *   }
 * )
 */
class EnrolManualEnrolUsers extends EndpointPluginBase {

}
