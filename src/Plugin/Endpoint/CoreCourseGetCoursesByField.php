<?php

namespace Drupal\moodle_connect\Plugin\Endpoint;

use Drupal\moodle_connect\Plugin\EndpointPluginBase;

/**
 * Plugin implementation of a moodle_connect endpoint.
 *
 * @Endpoint (
 *   id = "mcep_core_course_get_courses_by_field",
 *   label = @Translation("Get Courses By Field"),
 *   description = @Translation("Gets courses by field."),
 *   function = "core_course_get_courses_by_field",
 *   parameters = {
 *      "field" = "Field name",
 *      "value" = "Field value to search"
 *   }
 * )
 */
class CoreCourseGetCoursesByField extends EndpointPluginBase {

  public function response()
  {
    $response = parent::response();
    if (is_array($response) && !empty($response)) {
      $response = array_filter($response, function($course) {
        return $course->id > 1;
      });
    }
    return $response;
  }
}
