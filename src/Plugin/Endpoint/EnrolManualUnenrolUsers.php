<?php

namespace Drupal\moodle_connect\Plugin\Endpoint;

use Drupal\moodle_connect\Plugin\EndpointPluginBase;

/**
 * Plugin implementation of a moodle_connect endpoint.
 *
 * @Endpoint (
 *   id = "mcep_enrol_manual_unenrol_users",
 *   label = @Translation("Unerol User"),
 *   description = @Translation("Unenrols specified user from the specified course."),
 *   function = "enrol_manual_unenrol_users",
 *   parameters = {
 *      "userid" = "User ID",
 *      "courseid" = "Course ID"
 *   }
 * )
 */
class EnrolManualUnenrolUsers extends EndpointPluginBase {

}
