<?php

namespace Drupal\moodle_connect\Plugin\Endpoint;

use Drupal\moodle_connect\Plugin\EndpointPluginBase;

/**
 * Plugin implementation of a moodle_connect endpoint.
 *
 * @Endpoint (
 *   id = "mcep_core_user_get_users",
 *   label = @Translation("Get Users"),
 *   description = @Translation("Endpoint for retrieving users in Moodle."),
 *   function = "core_user_get_users",
 *   parameters = {
 *      "criteria" = "Criteria"
 *   }
 * )
 */
class CoreUserGetUsers extends EndpointPluginBase
{

}