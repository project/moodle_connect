<?php

namespace Drupal\moodle_connect\Plugin\Endpoint;

use Drupal\moodle_connect\Plugin\EndpointPluginBase;

/**
 * Plugin implementation of a moodle_connect endpoint.
 *
 * @Endpoint (
 *   id = "mcep_core_user_update_users",
 *   label = @Translation("Update Users"),
 *   description = @Translation("Endpoint for updating users in Moodle."),
 *   function = "core_user_update_users",
 *   parameters = {
 *      "users" = "Users"
 *   }
 * )
 */
class CoreUserUpdateUsers extends EndpointPluginBase
{

}