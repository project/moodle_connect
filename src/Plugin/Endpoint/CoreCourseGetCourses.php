<?php

namespace Drupal\moodle_connect\Plugin\Endpoint;

use Drupal\moodle_connect\Plugin\EndpointPluginBase;

/**
 * Plugin implementation of a moodle_connect endpoint.
 *
 * @Endpoint (
 *   id = "mcep_core_course_get_courses",
 *   label = @Translation("Get Courses"),
 *   description = @Translation("Gets available courses."),
 *   function = "core_course_get_courses",
 *   parameters = {}
 * )
 */
class CoreCourseGetCourses extends EndpointPluginBase {

  public function response()
  {
    $response = parent::response();
    if (is_array($response) && !empty($response)) {
      $response = array_filter($response, function($course) {
        return $course->id > 1;
      });
    }
    return $response;
  }
}
