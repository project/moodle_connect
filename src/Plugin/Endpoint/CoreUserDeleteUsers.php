<?php

namespace Drupal\moodle_connect\Plugin\Endpoint;

use Drupal\moodle_connect\Plugin\EndpointPluginBase;

/**
 * Plugin implementation of a moodle_connect endpoint.
 *
 * @Endpoint (
 *   id = "mcep_core_user_delete_users",
 *   label = @Translation("Delete Users"),
 *   description = @Translation("Endpoint for deleting users in Moodle."),
 *   function = "core_user_delete_users",
 *   parameters = {
 *      "userids" = "User IDs"
 *   }
 * )
 */
class CoreUserDeleteUsers extends EndpointPluginBase
{

}
