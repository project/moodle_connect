<?php

namespace Drupal\moodle_connect\Plugin\Endpoint;

use Drupal\moodle_connect\Plugin\EndpointPluginBase;

/**
 * Plugin implementation of a moodle_connect endpoint.
 *
 * @Endpoint (
 *   id = "mcep_core_course_get_contents",
 *   label = @Translation("Get Course Activities"),
 *   description = @Translation("Gets activities for the specified course."),
 *   function = "core_course_get_contents",
 *   parameters = {
 *      "courseid" = "Course ID"
 *   }
 * )
 */
class CoreCourseGetContents extends EndpointPluginBase {

}
