<?php

namespace Drupal\moodle_connect\Plugin\Endpoint;

use Drupal\moodle_connect\Plugin\EndpointPluginBase;

/**
 * Plugin implementation of a moodle_connect endpoint.
 *
 * @Endpoint (
 *   id = "mcep_core_user_create_users",
 *   label = @Translation("Create Users"),
 *   description = @Translation("Endpoint for creating users in Moodle."),
 *   function = "core_user_create_users",
 *   parameters = {
 *      "users" = "Users"
 *   }
 * )
 */
class CoreUserCreateUsers extends EndpointPluginBase
{

}