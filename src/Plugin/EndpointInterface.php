<?php

namespace Drupal\moodle_connect\Plugin;

/**
 * Interface for moodle-connect endpoint plugins.
 */
interface EndpointInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Returns the function name the resource represents.
   *
   * @return string
   */
  public function function() : string;

  /**
   * Returns the parameter-options (annotation) the resource takes/requires.
   *
   * @return array
   */
  public function parameters() : array;

  /**
   * Returns the parameter values to be used in the request.
   *
   * @return array
   */
  public function requestParameters() : array;

  /**
   * Handles resource request
   *
   * @return \stdClass|array|null
   */
  public function request();

  /**
   * Handles resource response
   *
   * @return \stdClass|array|null
   */
  public function response();

}
