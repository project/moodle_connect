<?php

namespace Drupal\moodle_connect\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\Menu\StaticMenuLinkOverridesInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MainMenu extends MenuLinkDefault implements ContainerFactoryPluginInterface {

  /** @var AccountInterface  */
  protected $current_user;

  /** @var RouteMatchInterface $route_match */
  protected RouteMatchInterface $route_match;

  /**
   * AccountMenu constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param array $plugin_definition
   * @param StaticMenuLinkOverridesInterface $static_override
   * @param AccountInterface $current_user
   * @param RouteMatchInterface $route_match
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, StaticMenuLinkOverridesInterface $static_override, AccountInterface $current_user, RouteMatchInterface $route_match)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $static_override);
    $this->current_user = User::load($current_user->id());
    $this->route_match = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu_link.static.overrides'),
      $container->get('current_user'),
      $container->get('current_route_match')
    );
  }

  public function isEnabled()
  {
    switch ($this->getRouteName()) {
      case 'moodle_connect.user.courses':
        if ($this->current_user->isAuthenticated()) {
          if ($this->current_user->hasPermission('moodle_connect can view user course list')) {
            return TRUE;
          } else if ($this->current_user->hasPermission('moodle_connect can view other users course list')) {
            if (($user_id = $this->route_match->getRawParameter('user')) && ($user_id !== $this->current_user->id())) {
              return TRUE;
            }
          }
        }
        return FALSE;
      default:
        return parent::isEnabled();
    }
  }

  public function getRouteParameters()
  {
    switch ($this->getRouteName()) {
      case 'moodle_connect.user.courses':
        return [
          'user' => $this->current_user->id(),
        ];
    }
    return parent::getRouteParameters();
  }

}
