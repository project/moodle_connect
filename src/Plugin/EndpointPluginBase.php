<?php

namespace Drupal\moodle_connect\Plugin;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\moodle_connect\Exception\EndpointException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for moodle-connect endpoint plugins.
 */
abstract class EndpointPluginBase extends PluginBase implements EndpointInterface, ContainerFactoryPluginInterface {

  protected $response;

  protected ConfigFactoryInterface $config_factory;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config_factory = $config_factory;

    $this->setConfiguration($configuration);
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getConfiguration() : array
  {
    return $this->configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array
  {
    return ['parameters' => []];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) : void
  {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function label() : string
  {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function function () : string
  {
    return (string) $this->pluginDefinition['function'];
  }

  /**
   * {@inheritdoc}
   */
  public function parameters() : array
  {
    return (array) $this->pluginDefinition['parameters'];
  }

  public function request()
  {
    $config = $this->config_factory->get('moodle_connect.settings');
    $baseurl = $config->get('url') . '/webservice/rest/server.php?';
    $params = [
      'wstoken' => $config->get('wstoken'),
      'wsfunction' => $this->function(),
      'moodlewsrestformat' => 'json',
    ] + $this->requestParameters();
    $url = $baseurl . http_build_query($params);
    $this->response = file_get_contents($url);
    return $this->response();
  }

  public function requestParameters(): array
  {
    // @TODO validate
    return $this->getConfiguration()['parameters'];
  }

  public function validateParameter(string $parameter, $value) : bool
  {
    return TRUE;
  }

  public function validateParameters(array &$errors = []) : bool
  {
    $parameter_args = $this->getConfiguration()['parameters'];
    $parameter_options = $this->parameters();

    if ($parameter_args && $parameter_options) {
      foreach ($parameter_options as $parameter => $parameter_specs) {
        if (isset($parameter_args[$parameter])) {
          if ($this->validateParameter($parameter, $parameter_args[$parameter])) {
            $request_parameters[$parameter] = $parameter_args[$parameter];
          }
        }
      }
    }
  }

  public function getRawResponse() : ?string
  {
    return $this->response;
  }

  public function response()
  {
    try {
      if ($this->response) {
        $response = json_decode($this->response);
        if (($error = json_last_error()) === JSON_ERROR_NONE) {
          if (!empty($response->exception)) {
            throw new EndpointException($this, $response->message);
          } else {
            return $response;
          }
        } else {
          throw new EndpointException($this,"Invalid response: {$error}");
        }
      }
      throw new EndpointException($this,"No response.");
    } catch (\Exception $e) {
      \Drupal::logger('moodle_connect')->error($e->getMessage());
      \Drupal::messenger()->addError("There was an error processing your request.  Please retry or contact an administrator.");
    }
    return null;
  }

}
