<?php

namespace Drupal\moodle_connect\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\moodle_connect\MoodleConnectTrait;
use Drupal\user\Entity\User;
use Symfony\Component\Routing\Route;

/**
 * Determines whether to show Moodle related workflows.
 */
class UserAccessCheck implements AccessInterface {

  use MoodleConnectTrait;

  protected AccountProxy $current_user;

  protected RouteMatchInterface $route_match;

  public function __construct(AccountProxy $current_user, RouteMatchInterface $route_match)
  {
    $this->current_user = $current_user;
    $this->route_match = $route_match;
  }

  /**
   * Access callback.
   *
   * @param Route $route
   *   The route to check against.
   *
   * @return AccessResultInterface
   *   The access result.
   */
  public function access(Route $route) {

    if (!$this->isEnabled()) {
      return AccessResult::forbidden();
    }

    if ($route->getRequirement('_access_check')) {
      $target_user = $this->getTargetUser();
      $is_self = $target_user->id() === $this->current_user->id();
      $allowed = AccessResult::forbidden();
      switch ($this->route_match->getRouteName()) {
        case 'moodle_connect.course_update_enrolment':
          if ($is_self && $this->current_user->hasPermission('moodle_connect can enroll')) {
            $allowed = AccessResult::allowed();
          } else if ($this->current_user->hasPermission('moodle_connect can enroll others')) {
            $allowed = AccessResult::allowed();
          }
          break;
        case 'entity.user.canonical':
        case 'entity.user.edit_form':
        case 'moodle_connect.user.courses':
          if ($is_self && $this->current_user->hasPermission('moodle_connect can view user course list')) {
            $allowed = AccessResult::allowed();
          } else if ($this->current_user->hasPermission('moodle_connect can view other users course list')) {
            if (!$is_self || $this->current_user->hasPermission('moodle_connect can enroll')) {
              $allowed = AccessResult::allowed();
            }
          }
          break;
      }
      return $allowed;
    }

    return AccessResult::forbidden();

  }

  /**
   * Returns the target-user, either extrapolated from route parameters or current user.
   *
   * @return EntityInterface|AccountProxy
   */
  protected function getTargetUser()
  {
    if (!($target_user = $this->route_match->getParameter('user'))) {
      $target_user = $this->current_user;
    }
    if (is_numeric($target_user)) {
      $target_user = User::load($target_user);
    }
    return $target_user;
  }

}
