<?php

namespace Drupal\moodle_connect\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines moodle-connect reset-resource annotation object.
 *
 * @Annotation
 */
class Endpoint extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $title;

  /**
   * The description of the plugin.
   *
   * @var Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $description;

  /**
   * The resource function the plugin supports.
   *
   * @var string
   */
  public string $function;

  /**
   * The resource parameters the plugin takes.
   *
   * @var array
   */
  public array $parameters = [];

}
