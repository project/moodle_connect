<?php

namespace Drupal\moodle_connect;

class Helper
{

  /**
   * @param string $message
   * @param array $context
   * @return void
   */
  public static function Debug(string $message, array $context = [])
  {
    if (self::DebugEnabled()) {
      \Drupal::logger('moodle_connect')->debug($message, $context);
    }
  }

  /**
   * @return bool
   */
  public static function DebugEnabled() : bool
  {
    $error_level = \Drupal::service('config.factory')->get('system.logging')->get('error_level');
    return ($error_level !== 'hide');
  }

  /**
   * Logs exception and displays user-friendly non-descript error.
   *
   * @param $exception
   * @return void
   */
  public static function HandleException($exception)
  {
    \Drupal::logger('moodle_connect')->error($exception->getMessage());
    \Drupal::messenger()->addError(t("There was an error processing your request.  Please retry or contact an administrator."));
    if (self::DebugEnabled()) {
      \Drupal::messenger()->addError("DEBUG: " . $exception->getMessage());
    }
  }

  /**
   * Returns log-level options.
   *
   * @return array
   */
  public static function GetLogLevels() : array
  {
    return [
      1 => t('Low - Changes to users or enrollments'),
      2 => t('High - Changes to users or enrollments and all requests'),
    ];
  }

  /**
   * Returns the appropriate log-level that pertains to the specified activity-type.
   *
   * @param string $activity_type
   * @return int
   */
  public static function GetActivityTypeLogLevel(string $activity_type) : int
  {
    switch ($activity_type) {
      case 'request':
        return 2;
      default:
        return 1;
    }
  }

}