(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.moodleConnectUpdateEnrollment = {
    attach: function (context, settings) {

      let updateEnrolment = (el, action) => {
        let moodleUserId = $(el).attr('moodle-user-id'),
            courseId = $(el).attr('course-id');
        Drupal.ajax({
          url: `/moodle/course/${action}/${moodleUserId}/${courseId}?returnUrl=` + encodeURIComponent(window.location.pathname),
        }).execute();
      };

      $('.moodle-course-links').once('moodleConnect').each(function() {
        $(document).on('click', "#moodle_enrol", function (event) {
          event.preventDefault();
          updateEnrolment(this, 'enrol');
        });

        $(document).on('click', "#moodle_unenrol", function (event) {
          event.preventDefault();
          updateEnrolment(this, 'unenrol');
        });
      });

    }
  };
})(jQuery, Drupal, drupalSettings);
